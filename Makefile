.PHONY: all kmod daemon ctrl redirfs		# prevent "nothing to be done for 'all'"

all: kmod daemon ctrl redirfs

kmod: 
	$(info BUILD $(@))
	make -C $@

daemon:
	$(info BUILD $(@))
	make -C $@

ctrl:
	$(info BUILD $(@))
	make -C $@

redirfs:
	$(info BUILD $(@))
	make -C $@
