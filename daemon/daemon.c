#include <fcntl.h>		// mq_open
#include <sys/stat.h>
#include <mqueue.h>
#include <stdlib.h>		// EXIT_FAILURE
#include <string.h>		// memset
#include <signal.h>		// sigevent
#include <stdio.h>		// perror
#include <pthread.h>	// pthread_create
#include <unistd.h>		// syscall
#include <sys/syscall.h>
#include <sys/ioctl.h>	// ioctl
#include <errno.h>		// errno

#define handle_error(msg) \
	do { perror(msg); return -1; } while (0)
//	do { perror(msg); exit(EXIT_FAILURE); } while (0)

// copied from ctrl.c
#define MSGQUEUE_NAME	"/WONGUKLEE"
#define MSGLEN			4096

mqd_t g_mqdes;
char g_msg[MSGLEN];

int g_redirfs_ko_fd;
int g_ko_fd;
int g_fd;

// copied from ctrl.c
#define ARGLEN			100
struct cmd {
	int type;
	int argc;
	char **argv;
};

// man mq_overview, man mq_notify
mqd_t create_mqueue(void) {
	struct mq_attr attr;
	memset(&attr, 0, sizeof(attr));

	attr.mq_flags = 0;			/* Message queue flags.  */
	attr.mq_maxmsg = 10;		/* Maximum number of messages.  */	
	attr.mq_msgsize = MSGLEN;	/* Maximum message size.  */
	attr.mq_curmsgs = 0;		/* Number of messages currently queued.  */

	// O_CREAT, so add mode and attr
	g_mqdes = mq_open(MSGQUEUE_NAME, O_CREAT | O_RDWR, 0644, &attr);
	if (g_mqdes == (mqd_t) -1)
		handle_error("mq_open");
}

int delete_mqueue(void) {
	if (mq_close(g_mqdes) == -1)
		handle_error("mq_close");

	if (mq_unlink(MSGQUEUE_NAME) == -1)
		handle_error("mq_unlink");
}

volatile int g_worker_running = 0;

static void copy_to_cmd(char *msg, struct cmd *c) {
	int i;

	memcpy(&(c->type), msg, sizeof(c->type));
	msg += sizeof(c->type);

	memcpy(&(c->argc), msg, sizeof(c->argc));
	msg += sizeof(c->argc);

	c->argv = calloc(1, sizeof(char **));

	for (i=0;i<c->argc;i++) {
		c->argv[i] = calloc(1, ARGLEN);		// not sizeof(char *)
		memcpy(c->argv[i], msg, ARGLEN);
		msg += ARGLEN;
	}
}

// copied from ctrl.c
enum {
	LOAD = 0,			// load kmod and open device file
	REGISTER,			// register kmod to redirfs
	RULE_ADD,
	RULE_DEL,
	RULE_LIST,
	STOP,				// stop daemon, kmod
	UNREGISTER,			// unregister kmod from redirfs
	UNLOAD,				// close device file and unload kmod
};

// copied from kmod.c
#define KMOD_MAGIC			(123)
#define KMOD_NR				(7)

#define IOCTL_REGISTER		_IO(KMOD_MAGIC, REGISTER)
#define IOCTL_START			_IO(KMOD_MAGIC, START)
#define IOCTL_RULE_ADD		_IO(KMOD_MAGIC, RULE_ADD)
#define IOCTL_RULE_DEL		_IO(KMOD_MAGIC, RULE_DEL)
#define IOCTL_RULE_LIST		_IO(KMOD_MAGIC, RULE_LIST)
#define IOCTL_STOP			_IO(KMOD_MAGIC, STOP)
#define IOCTL_UNREGISTER	_IO(KMOD_MAGIC, UNREGISTER)

#define REDIRFS_KO_FILE_PATH	"/root/demo/redirfs/redirfs.ko"
#define KO_FILE_PATH			"/root/demo/kmod/kmod.ko"
#define DEV_PATH				"/dev/kmod"
#define MODULE_NAME				"kmod"

#define REDIRFS_MODULE_PATH		"/sys/module/redirfs"

static int load_redirfs(void) {
	int ret;

	g_redirfs_ko_fd = open(REDIRFS_KO_FILE_PATH, O_RDONLY);	
	if (g_redirfs_ko_fd == -1) {
		printf("open fail %s (%d)\n", KO_FILE_PATH, errno);	

		g_redirfs_ko_fd = 0;
		return -1;
	}

	ret = syscall(__NR_finit_module, g_redirfs_ko_fd, "", 0);
	if (ret == -1) {
		printf("finit_module fail (%d)\n", errno);

		close(g_redirfs_ko_fd);
		g_redirfs_ko_fd = 0;
		return -1;
	}

	return 0;
}

static void do_cmd(struct cmd *c) {
	int ret;
	int unreg_fd;

	struct stat tmp;

	switch (c->type) {
	case LOAD:
		printf("load\n");

		if (lstat(REDIRFS_MODULE_PATH, &tmp) == -1 && errno == ENOENT) {
			ret = load_redirfs();
			if (ret < 0) {
				return;
			}
		}

		g_ko_fd = open(KO_FILE_PATH, O_RDONLY);
		if (g_ko_fd == -1) {
			printf("open fail %s (%d)\n", KO_FILE_PATH, errno);	
			g_ko_fd = 0;
			return;
		}

		//#define __NR_finit_module 273
		ret = syscall(__NR_finit_module, g_ko_fd, "", 0);
		if (ret == -1) {
			printf("finit_module fail (%d)\n", errno);
			return;
		}

		g_fd = open(DEV_PATH, O_RDWR);
		if (g_fd == -1) {
			printf("open fail %s (%d)\n", DEV_PATH, errno);
			g_fd = 0;
			return;
		}
		break;

	case REGISTER:
		ioctl(g_fd, IOCTL_REGISTER, NULL);
		break;

	case RULE_ADD:
		ioctl(g_fd, IOCTL_RULE_ADD, NULL);
		break;

	case RULE_DEL:
		ioctl(g_fd, IOCTL_RULE_DEL, NULL);
		break;

	case RULE_LIST:
		ioctl(g_fd, IOCTL_RULE_LIST, NULL);
		break;

	case STOP:
		ioctl(g_fd, IOCTL_STOP, NULL);
		break;

	case UNREGISTER:
		printf("unregister\n");
		system("echo 1 > /sys/fs/redirfs/filters/kmod/unregister");
		sleep(0.1);

		ioctl(g_fd, IOCTL_UNREGISTER, NULL);
		break;

	case UNLOAD:
		printf("unload\n");

		if (g_fd)
			close(g_fd);

		//delete_module(MODULE_NAME, 0);
		//#define __NR_delete_module 106
		syscall(__NR_delete_module, MODULE_NAME, 0);

		if (g_ko_fd) {
			close(g_ko_fd);
			g_ko_fd = 0;
		}

		if (g_redirfs_ko_fd) {
			close(g_redirfs_ko_fd);
			g_redirfs_ko_fd = 0;
		}

		break;

	default:
		break;
	}
}

static void *handler(void *arg) {
	ssize_t res;
	struct timespec ts;
	struct cmd c = {0,};
	int i;

	while (g_worker_running) {
		clock_gettime(CLOCK_REALTIME, &ts);		// set block time of mq_timedreceive
		ts.tv_nsec += 1000000;		// 1ms

		res = mq_timedreceive(g_mqdes, g_msg, MSGLEN, NULL, &ts);
		if (res == -1)
			continue;

		copy_to_cmd(g_msg, &c);

		printf("[msg] type : %d,  argc : %d,  argv\n", c.type, c.argc);
		for (i=0;i<c.argc;i++) {
			printf(" [%d] %s\n", i, c.argv[i]);
		}	
		printf("\n");

		do_cmd(&c);

		// calloc free (no longer need)
		for (i=0;i<c.argc;i++) {
			free(c.argv[i]);
		}
		free(c.argv);
		c.argv = NULL;			// just in case
	}

	printf("terminated\n");
}

pthread_t create_worker(void *arg) {
	pthread_t tid;
	int res;

	g_worker_running = 1;
	res = pthread_create(&tid, NULL, &handler, arg);
	if (res != 0)
		handle_error("pthread_create");

	return tid;
}

int delete_worker(pthread_t tid) {
	int res;

	g_worker_running = 0;
	res = pthread_join(tid, NULL);
	if (res != 0)
		handle_error("pthread_join");

	return res;
}

int main(int argc, char argv[]) {
	struct sigaction ac;
	int res;
	pthread_t tid;

	g_mqdes = create_mqueue();
	if (g_mqdes == (mqd_t) -1)
		handle_error("create_mqeue");

	tid = create_worker(NULL);
	if (tid < 0)
		goto err;

	// stop temporarily
	getchar();

	res = delete_worker(tid);
	if (res < 0)
		goto err;

	if (delete_mqueue() == -1)
		handle_error("delete_mqueue");

	return 0;

err:
	return 1;
}
