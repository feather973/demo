#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>


#define handle_error(msg) \
	do { perror(msg); return -1; } while (0)

#define MSGQUEUE_NAME	"/WONGUKLEE"
#define	MSGLEN			4096

enum {
	LOAD = 0,			// load kmod
	REGISTER,			// register kmod to redirfs
	RULE_ADD,
	RULE_DEL,
	RULE_LIST,
	STOP,				// stop daemon, kmod
	UNREGISTER,			// unregister kmod from redirfs
	UNLOAD,				// unload kmod
};

#define CMDLEN			100
struct cmd_format {
	int type;
	char cmd[CMDLEN];
	int argc;
};

struct cmd_format g_cmd_format[] = {
	{ LOAD,			"load",			0 },
	{ REGISTER,		"register",		0 },
	{ RULE_ADD,		"rule_add",		2 },
	{ RULE_DEL,		"rule_del",		2 },
	{ RULE_LIST,	"rule_list",	1 },
	{ STOP,			"stop",			0 },
	{ UNREGISTER,	"unregister",	0 },
	{ UNLOAD,		"unload",		0 },
};

#define ARGLEN			100
struct cmd {
	int type;
	int argc;
	char **argv;	
};

int parse_argv(int argc, char *argv[], struct cmd *c) {
	int i;

	if (argc == 1)
		goto err;

	if (!strcmp(argv[1], g_cmd_format[LOAD].cmd))
		c->type = LOAD;
	else if (!strcmp(argv[1], g_cmd_format[REGISTER].cmd))
		c->type = REGISTER;
	else if (!strcmp(argv[1], g_cmd_format[RULE_ADD].cmd))
		c->type = RULE_ADD;
	else if (!strcmp(argv[1], g_cmd_format[RULE_DEL].cmd))
		c->type = RULE_DEL;
	else if (!strcmp(argv[1], g_cmd_format[RULE_LIST].cmd))
		c->type = RULE_LIST;
	else if (!strcmp(argv[1], g_cmd_format[STOP].cmd))
		c->type = STOP;
	else if (!strcmp(argv[1], g_cmd_format[UNREGISTER].cmd))
		c->type = UNREGISTER;
	else if (!strcmp(argv[1], g_cmd_format[UNLOAD].cmd))
		c->type = UNLOAD;
	else
		goto err;

	c->argc = g_cmd_format[c->type].argc;

	// argv[0] : program name,  argv[1] : c->type,  argv[2] : first argument
	// ex) RULE_LIST
	//   argc 3
	//   c->argc 1
	argc -= 2;		
	if (c->argc != argc)
		goto err;

	c->argv = calloc(1, sizeof(char **));		// memory allocated other place

	for (i=0;i<c->argc;i++) {
		c->argv[i] = calloc(1, ARGLEN);
		memcpy(c->argv[i], argv[i+2], ARGLEN);	// argv starts [2]
	}

	return 0;
	
err:
	errno = EINVAL;
	return -1;
}

void copy_to_msg(struct cmd *c, char *msg) {
	int i;

	memcpy(msg, &(c->type), sizeof(c->type));
	msg += sizeof(c->type);

	memcpy(msg, &(c->argc), sizeof(c->argc));
	msg += sizeof(c->argc);

	for (i=0;i<c->argc;i++) {
		memcpy(msg, c->argv[i], ARGLEN);		// fixed length
		msg += ARGLEN;
	}
}

int main(int argc, char *argv[]) {
	struct cmd c;
	mqd_t mqdes;
	char msg[MSGLEN] = {0,};					// resticted memory (if ARGLEN too big, stack smash occurs)

	ssize_t res;
	int i;

	mqdes = mq_open(MSGQUEUE_NAME, O_RDWR);
	if (mqdes == (mqd_t) -1)
		handle_error("mq_open");

	res = parse_argv(argc, argv, &c);
	if (res == -1)
		handle_error("parse_argv");

	// no need for serialization (same network)
	copy_to_msg(&c, msg);

	// calloc free (no longer need)
	for (i=0;i<c.argc;i++) {
		free(c.argv[i]);
	}
	free(c.argv);

	res = mq_send(mqdes, msg, MSGLEN, 1);		// 0(low) to sysconf(_SC_MQ_PRIO_MAX)
	if (res == -1)
		handle_error("mq_send");

	mq_close(mqdes);

	return 0;
}
