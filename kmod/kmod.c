#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kallsyms.h>
#include <linux/kprobes.h>

#define DEVICE_NAME		"kmod"

#define KMOD_MAGIC          (123)
#define KMOD_NR             (7)

// copied from ctrl.c
enum {
	LOAD = 0,			// load kmod and open device file
	REGISTER,			// register kmod to redirfs
	RULE_ADD,
	RULE_DEL,
	RULE_LIST,
	STOP,				// stop daemon, kmod
	UNREGISTER,			// unregister kmod from redirfs
	UNLOAD,				// close device file and unload kmod
};

#define IOCTL_REGISTER      _IO(KMOD_MAGIC, REGISTER)
#define IOCTL_RULE_ADD      _IO(KMOD_MAGIC, RULE_ADD)
#define IOCTL_RULE_DEL      _IO(KMOD_MAGIC, RULE_DEL)
#define IOCTL_RULE_LIST     _IO(KMOD_MAGIC, RULE_LIST)
#define IOCTL_STOP          _IO(KMOD_MAGIC, STOP)
#define IOCTL_UNREGISTER    _IO(KMOD_MAGIC, UNREGISTER)

dev_t g_id;
struct cdev g_cdev;
struct class *g_class;
struct device *g_dev;

#include "../redirfs/redirfs.h"
typedef redirfs_filter (*redirfs_register_filter_t)(struct redirfs_filter_info *info);
typedef int (*redirfs_set_operations_t)(redirfs_filter filter, struct redirfs_op_info ops[]);
typedef void (*redirfs_delete_filter_t)(redirfs_filter filter);

typedef unsigned long (*kallsyms_lookup_name_t)(const char *name);
kallsyms_lookup_name_t call_kallsyms_lookup_name;

redirfs_register_filter_t		call_redirfs_register_filter;
redirfs_set_operations_t		call_redirfs_set_operations;
redirfs_delete_filter_t			call_redirfs_delete_filter;

struct redirfs_filter_operations g_flt_ops = {0,};

struct redirfs_filter_info g_flt_info = {
	.owner = THIS_MODULE,
	.name = DEVICE_NAME,
	.priority = 100,
	.active = 0,
	.ops = &g_flt_ops,
};

redirfs_filter g_filter;

// printk message doesn't display till next event
// https://askubuntu.com/questions/1111529/printk-message-doesnt-display-till-next-event

enum redirfs_rv kmod_pre_open_handler(void *, struct redirfs_args *) {
	printk("pre open\n");

	return REDIRFS_CONTINUE;
}

enum redirfs_rv kmod_post_open_handler(void *, struct redirfs_args *) {
	printk("post open\n");

	return REDIRFS_CONTINUE;
}

enum redirfs_rv kmod_pre_release_handler(void *, struct redirfs_args *) {
	printk("pre release\n");

	return REDIRFS_CONTINUE;
}

enum redirfs_rv kmod_post_release_handler(void *, struct redirfs_args *) {
	printk("post release\n");

	return REDIRFS_CONTINUE;
}

struct redirfs_op_info g_ops [] = {
	{
		.op_id = REDIRFS_REG_FOP_OPEN,
		.pre_cb = kmod_pre_open_handler,
		.post_cb = kmod_post_open_handler,
	},
	{
		.op_id = REDIRFS_REG_FOP_RELEASE,
		.pre_cb = kmod_pre_release_handler,
		.post_cb = kmod_post_release_handler,
	},
	{
		.op_id = REDIRFS_OP_END,
	}
};

long kmod_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
	int ret;

	if (_IOC_TYPE(cmd) != KMOD_MAGIC)
		return -EINVAL;

	if (_IOC_NR(cmd) >= KMOD_NR)
		return -EINVAL;

	switch (cmd) {
	case IOCTL_REGISTER:
		printk("register\n");
		//printk("%llx\n", (long long unsigned int) call_redirfs_register_filter);
		//printk("%llx\n", (long long unsigned int) call_redirfs_unregister_filter);

		g_filter = call_redirfs_register_filter(&g_flt_info);	
		if (IS_ERR(g_filter)) {
			printk("call_redirfs_register_filter\n");	

			return 0;
		}

		// register 
		// root@user-virtual-machine:/sys/fs/redirfs/filters/kmod# ls
		// active  paths  priority  unregister
		ret = call_redirfs_set_operations(g_filter, g_ops);
		if (ret != 0) {
			printk("call_redirfs_set_operations\n");

			return 0;
		}

		// add test path
		
		break;
	case IOCTL_RULE_ADD:
		printk("rule_add\n");
		break;
	case IOCTL_RULE_DEL:
		printk("rule_del\n");
		break;
	case IOCTL_RULE_LIST:
		printk("rule_list\n");
		break;
	case IOCTL_STOP:
		printk("stop\n");
		break;
	case IOCTL_UNREGISTER:
		printk("unregister\n");

		call_redirfs_delete_filter(g_filter);
		g_filter = NULL;

		break;

	default:
		break;

	}

	return 0;
}

// kallsyms_lookup_name is not exported anymore in kernels > 5.7
// https://github.com/xcellerator/linux_kernel_hacking/issues/3
void find_kallsyms(void)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,7,0)
	static struct kprobe kp = {
		.symbol_name = "kallsyms_lookup_name"
	};

	register_kprobe(&kp);
	call_kallsyms_lookup_name = (kallsyms_lookup_name_t) kp.addr;
	unregister_kprobe(&kp);
#else
	call_kallsyms_lookup_name = kallsyms_lookup_name;
#endif
}

int kmod_kallsyms(void)
{
	call_redirfs_register_filter = 
		(redirfs_register_filter_t) call_kallsyms_lookup_name("redirfs_register_filter");	
	if (!call_redirfs_register_filter) {
		printk("redirfs_register_filter");
		return 1;
	}

	call_redirfs_set_operations = 
		(redirfs_set_operations_t) call_kallsyms_lookup_name("redirfs_set_operations");
	if (!call_redirfs_set_operations) {
		printk("redirfs_set_operations");
		return 1;
	}

	call_redirfs_delete_filter = 
		(redirfs_delete_filter_t) call_kallsyms_lookup_name("redirfs_delete_filter");
	if (!call_redirfs_delete_filter) {
		printk("redirfs_set_operations");
		return 1;
	}

	return 0;
}

int kmod_open(struct inode *inode, struct file *file)
{
	printk("open");
	return 0;
}

int kmod_release(struct inode *inode, struct file *file)
{
	printk("release");
	return 0;
}

ssize_t kmod_read(struct file *filp, char __user *buf, size_t len, loff_t *ppos)
{
	printk("read");
	return len;
}

ssize_t kmod_write(struct file *filp, const char __user *buf, size_t len, loff_t *ppos)
{
	printk("write");
	return len;
}

struct file_operations kmod_fops = {
	.owner				= THIS_MODULE,
	.read				= kmod_read,
	.write				= kmod_write,
	.unlocked_ioctl		= kmod_ioctl,
	.open				= kmod_open,
	.release			= kmod_release,
};

static int create_cdev(void) {
	int ret;

	ret = alloc_chrdev_region(&g_id, 0, 1, DEVICE_NAME);
	if (ret) {
		printk("alloc_chrdev_region");
		return ret;
	}

	cdev_init(&g_cdev, &kmod_fops);
	g_cdev.owner = THIS_MODULE;

	ret = cdev_add(&g_cdev, g_id, 1);
	if (ret) {
		printk("cdev_add");

		unregister_chrdev_region(g_id, 1);
		return ret;
	}

	g_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(g_class)) {
		ret = PTR_ERR(g_class);
		printk("class_create");

		cdev_del(&g_cdev);
		unregister_chrdev_region(g_id, 1);
		return ret;
	}

	g_dev = device_create(g_class, NULL, g_id, NULL, DEVICE_NAME);
	if (IS_ERR(g_dev)) {
		ret = PTR_ERR(g_dev);
		printk("device_create");

		class_destroy(g_class);
		cdev_del(&g_cdev);
		unregister_chrdev_region(g_id, 1);
		return ret;
	}

	return 0;
}

static void delete_cdev(void) {
	device_destroy(g_class, g_id);

	class_destroy(g_class);
	cdev_del(&g_cdev);
	unregister_chrdev_region(g_id, 1);
}

int init_kmod(void) {
	create_cdev();

	find_kallsyms();
	kmod_kallsyms();

	return 0;
}

void exit_kmod(void) {
	delete_cdev();

	return;
}

module_init(init_kmod);
module_exit(exit_kmod);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wonguk Lee");
MODULE_VERSION("1.0");
