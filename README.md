# demo
redirfs test filter
- get open/close event
- deny open/close event

## COMPONENT
daemon : handle ipc messages  
ctrl : send ipc messages  
kmod : registered to redirfs  
redirfs : redirfs (from https://github.com/cisco/redirfs.git)

## COMMAND
LOAD  
START  
RULE ADD  
RULE DEL  
RULE LIST  
STOP  
UNLOAD  
